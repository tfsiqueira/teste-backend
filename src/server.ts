import 'reflect-metadata';

import express, { Request, Response, NextFunction }  from 'express';
import 'express-async-errors';

import './database';
import usersRouter from './modules/users/routes/users.routes';
import sessionsRouter from './modules/users/routes/sessions.routes';
import ExceptionError from './shared/errors/ExceptionError';
import peoplesRouter from './modules/peoples/routes/peoples.routes';
import moviesRouter from './modules/movies/routes/movies.routes';

const app = express();
app.use(express.json());

//Routes
app.use(usersRouter);
app.use(sessionsRouter);
app.use(peoplesRouter);
app.use(moviesRouter);

//Errors
app.use((err: Error, request: Request, response: Response, next: NextFunction) => {
  if (err instanceof ExceptionError) {
    return response.status(err.statusCode).json({
      status: 'error',
      message: err.message,
    });
  }

  console.error(err);

  return response.status(500).json({
      status: 'error',
      message: 'Internal Server Error',
  });
});

//Start Server
app.listen(process.env.PORT, () => {
  console.log(`Server it's Running on port: ${process.env.PORT}`);
})
