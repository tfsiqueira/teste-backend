import { createConnection } from 'typeorm';
import * as path from 'path';

createConnection({
  "type": "postgres",
	"host": "localhost",
  "port": 5432,
  "username": process.env.TYPEORM_USERNAME,
  "password": process.env.TYPEORM_PASSWORD,
  "database": process.env.TYPEORM_DATABASE,
  "entities": [path.join(__dirname, '../modules/**/*{.ts,.js}')],
  "migrations": [path.join(__dirname, '/migrations/*{.ts,.js}')],
	"cli": {
		"migrationsDir": path.join(__dirname, '/migrations')
	}
})
