import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateMovies1627709345544 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
      name: 'movies',
      columns: [
        {
          name: 'id',
          type: 'uuid',
          isPrimary: true,
          generationStrategy: 'uuid',
          default: 'uuid_generate_v4()'
        },
        {
          name: 'title',
          type: 'varchar',
        },
        {
          name: 'genre',
          type: 'varchar',
        },
        {
          name: 'synopsis',
          type: 'varchar',
        },
        {
          name: 'release',
          type: 'timestamp',
        },
        {
          name: 'running_time',
          type: 'numeric',
        },
        {
          name: 'rating',
          type: 'decimal',
          isNullable: true
        },
        {
          name: 'numer_of_rating',
          type: 'numeric',
          isNullable: true
        },
        {
          name: 'director_id',
          type: 'uuid',
        },
        {
          name: 'created_at',
          type: 'timestamp',
          default: 'now()'
        },
        {
          name: 'updated_at',
          type: 'timestamp',
          default: 'now()'
        }
      ],
      foreignKeys: [
        {
          name: 'FKDirector',
          referencedTableName: 'peoples',
          referencedColumnNames: ['id'],
          columnNames: ['director_id'],
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE'
        }
      ]
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('movies');
  }

}
