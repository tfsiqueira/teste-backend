import People from '../../peoples/models/People';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  ManyToOne,
  JoinColumn,
  RelationId,
  JoinTable
} from 'typeorm';

@Entity('movies')
export default class Movie {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  title: string;

  @Column('varchar')
  genre: string;

  @Column('varchar')
  synopsis: string;

  @Column('date')
  release: Date;

  @Column('numeric', { name: 'running_time' })
  runningTime: number;

  @ManyToOne(type => People, people => people.directedMovies)
  @JoinColumn({ name: 'director_id'})
  director: People;

  @RelationId((movie: Movie) => movie.director)
  directorId: string;

  @ManyToMany(type => People)
  @JoinTable({
    joinColumn: { name: 'movie_id' },
    inverseJoinColumn: { name: 'actor_id' },
  })
  actores: People[];

  @RelationId((movie: Movie) => movie.actores)
  actoresIds: string[];

  @Column('decimal')
  rating?: number;

  @Column('numeric', { name: 'numer_of_rating' })
  numberOfRating?: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
