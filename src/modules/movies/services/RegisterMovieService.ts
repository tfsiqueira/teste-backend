import { getRepository } from "typeorm";
import { v4 as uuid } from "uuid";
import { IRegisterMovieService } from "../dtos/IRegisterMovies.dto";
import Movie from "../models/Movie";

export default class RegisterMovieService {
  async create(
    {title, genre, synopsis, director, actores, release, runningTime}: IRegisterMovieService
  ): Promise<Movie> {
    const moviesRepository = getRepository(Movie);

    const movieData = {
      id: uuid(),
      title,
      genre,
      synopsis,
      release: new Date(release),
      runningTime,
      director,
      actores
    };

    const movie = moviesRepository.create(movieData);
    await moviesRepository.save(movie);

    return movie;
  }
}
