import { getCustomRepository } from "typeorm"
import { IFindMoviesByFiltersDTO } from "../dtos/IFindMoviesByFilters.dto";
import { MoviesRepository } from "../repositories/MoviesRepository"

export default class SearchMovieService {
  async getMovies({title, director, genre, actor, page}: IFindMoviesByFiltersDTO) {

    if (page === 0 || !page) {
      page = 1;
    }

    const take = 10;
    const skip = (page * 10) - 10;

    const moviesRepository = getCustomRepository(MoviesRepository);

    const result =  await moviesRepository.findByFilters({
      title,
      director,
      genre,
      actor
    }, take, skip);

    return {
      data: result.data,
      count: result.count,
      current: result.data.length,
      page: Number(page)
    };
  }
}
