import ExceptionError from "../../../shared/errors/ExceptionError";
import IRatingMovie from "../dtos/IRatingMovie.dto";
import Movie from "../models/Movie";
import { MoviesRepository } from "../repositories/MoviesRepository";

export default class RatingMovieService {
  async rating({movieId, rate}: IRatingMovie): Promise<Movie> {
    const moviesRepository = new MoviesRepository();

    let movie = await moviesRepository.findById(movieId);

    if (!movie) {
      throw new ExceptionError('Movie not found', 404);
    }

    let numberOfRating = movie.numberOfRating;
    let rating = movie.rating;

    if (!numberOfRating) {
      numberOfRating = 0;
    }

    numberOfRating = Number(numberOfRating) + 1;

    if (rating) {
      rating = (Number(rating) + Number(rate));
    } else {
      rating = Number(rate);
    }

    movie.numberOfRating = numberOfRating;
    movie.rating = rating;

    movie = await moviesRepository.save(movie);

    return movie;
  }
}
