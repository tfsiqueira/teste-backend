import { Router } from 'express';
import ensureAuthenticated from '../../users/middlewares/ensureAuthenticated';
import { userIsAdmin } from '../../users/middlewares/userIsAdmin';
import { userIsUserRole } from '../../users/middlewares/userIsUserRole';
import GetInfoMovieController from '../controller/GetInfoMovieController';
import MovieController from '../controller/MovieController';
import RatingMovieController from '../controller/RatingMovieController';
import SearchMovieController from '../controller/SearchMovieController';

const moviesRouter = Router();
const movieController = new MovieController();
const searchMovieController = new SearchMovieController();
const getInfoMovieController = new GetInfoMovieController();
const ratingMovieController = new RatingMovieController();

moviesRouter.post('/movies/register',
  ensureAuthenticated,
  userIsAdmin,
  movieController.create);

moviesRouter.get('/movies', searchMovieController.search);
moviesRouter.get('/movies/:id', getInfoMovieController.show);
moviesRouter.post('/movies/:id/rating',
  ensureAuthenticated,
  userIsUserRole,
  ratingMovieController.rating);

export default moviesRouter;
