import { Request, Response } from 'express';
import { IFindMoviesByFiltersDTO } from '../dtos/IFindMoviesByFilters.dto';
import SearchMovieService from '../services/SearchMovieService';

export default class SearchMovieController {
  async search(request: Request, response: Response): Promise<Response> {
    const {
      title,
      director,
      genre,
      actor,
      page
    } = request.query as unknown as IFindMoviesByFiltersDTO;

    const searchMovieService = new SearchMovieService();

    const moviesData = await searchMovieService.getMovies({
      title,
      director,
      genre,
      actor,
      page
    });

    return response.json(moviesData);
  }
}
