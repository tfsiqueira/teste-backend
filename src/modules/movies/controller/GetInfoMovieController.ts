import { Request, Response } from 'express';
import ExceptionError from '../../../shared/errors/ExceptionError';
import { MoviesRepository } from '../repositories/MoviesRepository';

export default class GetInfoMovieController {
  async show(request: Request, response: Response): Promise<Response> {
    const movieId = request.params.id;

    const moviesRepository = new MoviesRepository();

    const movie = await moviesRepository.findById(movieId);

    if (!movie) {
      throw new ExceptionError('Movie not Found', 404);
    }

    const payload = {
      id: movie.id,
      title: movie.title,
      genre: movie.genre,
      director: movie.director,
      synopsis: movie.synopsis,
      release: movie.release,
      runningTime: movie.runningTime,
      rating: movie.rating
      ? Number(Number(movie.rating)/Number(movie.numberOfRating)).toFixed(2)
      : null,
      numberOfRating: movie.numberOfRating,
      actores: movie.actores
    }

    return response.json(payload);
  }
}
