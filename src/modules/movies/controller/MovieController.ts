import { Request, Response } from 'express';
import { getCustomRepository, getRepository } from 'typeorm';
import ExceptionError from '../../../shared/errors/ExceptionError';
import PeoplesRepository from '../../peoples/repositories/PeoplesRepository';
import Movie from '../models/Movie';
import RegisterMovieService from '../services/RegisterMovieService';

export default class MovieController {
  async create(request: Request, response: Response): Promise<Response> {
    const {
      title,
      genre,
      synopsis,
      release,
      runningTime,
      directorId,
      actoresIds
    } = request.body;

    const peoplesRepository = getCustomRepository(PeoplesRepository);
    const moviesRepository = getRepository(Movie);
    const registerMovieService = new RegisterMovieService();

    const director = await peoplesRepository.findById(directorId);

    if (! director) {
      throw new ExceptionError('The film must have a director');
    }

    const existMovie = await moviesRepository.findOne({ where: {
      title, director
    }});

    if (existMovie) {
      throw new ExceptionError('This movie is already registered');
    }

    const actores = await peoplesRepository.findManyPeoples(actoresIds);

    if (actores.length === 0) {
      throw new ExceptionError('The film must have a Actor');
    }

    const movie = await registerMovieService.create({
      title, genre, synopsis, director, actores, release, runningTime
    });

    return response.status(201).json(movie);
  }
}
