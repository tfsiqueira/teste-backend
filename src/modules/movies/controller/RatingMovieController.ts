import { Request, Response } from 'express';
import ExceptionError from '../../../shared/errors/ExceptionError';
import RatingMovieService from '../services/RatingMovieService';

export default class RatingMovieController {
  async rating(request: Request, response: Response): Promise<Response> {
    const movieId = request.params.id;

    const { rate } = request.body;

    if (Number(rate) < 0 || Number(rate) > 4) {
      throw new ExceptionError('Invalid Value. Give a grade between 0 to 4');
    }

    const ratingMovieService = new RatingMovieService();

    const movie = await ratingMovieService.rating({movieId, rate});

    const payload = {
      id: movie.id,
      title: movie.title,
      genre: movie.genre,
      director: movie.director,
      synopsis: movie.synopsis,
      release: movie.release,
      runningTime: movie.runningTime,
      rating: movie.rating
        ? Number(Number(movie.rating)/Number(movie.numberOfRating)).toFixed(2)
        : null,
      numberOfRating: movie.numberOfRating,
      actores: movie.actores
    }

    return response.status(201).json(payload);
  }
}
