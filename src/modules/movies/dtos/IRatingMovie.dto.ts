export default interface IRatingMovie {
  movieId: string;
  rate: number;
}
