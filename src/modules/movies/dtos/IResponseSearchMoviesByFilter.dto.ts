import Movie from "../models/Movie";

export default interface IResponseSearchMoviesByFilter {
  data: Movie[];
  count: number;
}
