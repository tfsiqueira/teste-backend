import People from "../../peoples/models/People";

export interface IRegisterMovieService {
  title: string;
  genre: string;
  synopsis: string;
  release: string;
  runningTime: number;
  director: People;
  actores: People[];
}
