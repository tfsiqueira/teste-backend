export interface IFindMoviesByFiltersDTO {
  title?: string | undefined;
  director?: string | undefined;
  genre?: string | undefined;
  actor?: string | undefined;
  page: number;
}
