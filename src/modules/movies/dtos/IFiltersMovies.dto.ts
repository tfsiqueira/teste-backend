export interface IFilterMoviesDTO {
  title?: string | undefined;
  director?: string | undefined;
  genre?: string | undefined;
  actor?: string | undefined;
}
