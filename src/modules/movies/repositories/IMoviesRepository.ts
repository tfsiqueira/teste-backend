import { IFilterMoviesDTO } from "../dtos/IFiltersMovies.dto";
import IResponseSearchMoviesByFilter from "../dtos/IResponseSearchMoviesByFilter.dto";
import Movie from "../models/Movie";

export default interface IMoviesRepository {
  findByFilters(
    filters: IFilterMoviesDTO,
    take: number,
    skip: number
  ): Promise<IResponseSearchMoviesByFilter>;
  findById(id: string): Promise<Movie | undefined>;
  save(movie: Movie): Promise<Movie>;
}
