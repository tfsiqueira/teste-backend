import { EntityRepository, getRepository, Repository } from "typeorm";
import { IFilterMoviesDTO } from "../dtos/IFiltersMovies.dto";
import IResponseSearchMoviesByFilter from "../dtos/IResponseSearchMoviesByFilter.dto";
import Movie from "../models/Movie";
import IMoviesRepository from "./IMoviesRepository";

@EntityRepository(Movie)
export class MoviesRepository implements IMoviesRepository {
  private ormRepository: Repository<Movie>;

  constructor() {
    this.ormRepository = getRepository(Movie);
  }

  async findByFilters(
    filters: IFilterMoviesDTO,
    take: number,
    skip: number
  ): Promise<IResponseSearchMoviesByFilter> {
    const { title, director, genre, actor } = filters;

    const selectQueryBuilder = this.ormRepository.createQueryBuilder('movies')
      .leftJoinAndSelect('movies.actores', 'actores')
    if (title) {
      selectQueryBuilder.where('LOWER(movies.title) LIKE :title',
        { title: `%${ title.toLowerCase() }%`}
      );
    }

    if (genre) {
      selectQueryBuilder.andWhere('movies.genre = :genre', { genre });
    }

    if (director) {
      selectQueryBuilder.andWhere('movies.director_id = :director', { director })
    }

    if (actor) {
      selectQueryBuilder.andWhere('actores.id = :actor', { actor })
    }

    const data = await selectQueryBuilder
      .orderBy('movies.title', 'ASC')
      .take(take)
      .skip(skip)
      .getMany();

    const count = await selectQueryBuilder.getCount();

    return {data, count};
  }

  async findById(id: string): Promise<Movie | undefined> {
    const movie = await this.ormRepository.findOne(id, {
      relations: ['director', 'actores']
    });

    return movie;
  }

  async save(movie: Movie): Promise<Movie> {
    return await this.ormRepository.save(movie);
  }
}
