import { Router } from 'express';
import ensureAuthenticated from '../../users/middlewares/ensureAuthenticated';
import RegisterPeopleController from '../controllers/RegisterPeopleController';
import SearchPeopleController from '../controllers/SearchPeopleController';

const peoplesRouter = Router();
const peopleController = new RegisterPeopleController();
const searchPeopleController = new SearchPeopleController();

peoplesRouter.post('/peoples/register',
  ensureAuthenticated,
  peopleController.register
);

peoplesRouter.get('/peoples', searchPeopleController.searchByName);

export default peoplesRouter;
