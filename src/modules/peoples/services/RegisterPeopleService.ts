import ExceptionError from "../../../shared/errors/ExceptionError";
import IRegisterPeopleDTO from "../dtos/IRegisterPeople.dto";
import People from "../models/People";
import PeoplesRepository from "../repositories/PeoplesRepository";

export default class RegisterPeopleService {
  async execute({name, age, document}: IRegisterPeopleDTO): Promise<People> {
    const peoplesRepository = new PeoplesRepository();

    const peopleAlreadyExist = await peoplesRepository.findByDocument(document);

    if (peopleAlreadyExist) {
      throw new ExceptionError('People already registered!');
    }

    const people = await peoplesRepository.create({
      name,
      age,
      document
    });

    return people;
  }
}
