import People from "../models/People";
import PeoplesRepository from "../repositories/PeoplesRepository";

export default class SearchByNameService {
  async execute(name: string): Promise<People[] | undefined> {
    const peoplesRepository = new PeoplesRepository();

    const peoples = await peoplesRepository.findByName(name);

    return peoples;
  }
}
