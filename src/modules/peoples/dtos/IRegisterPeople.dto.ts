export default interface IRegisterPeopleDTO {
  name: string;
  age: number;
  document: string;
}
