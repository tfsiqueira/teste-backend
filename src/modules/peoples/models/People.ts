import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import Movie from "../../movies/models/Movie";

@Entity('peoples')
export default class People {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  name: string;

  @Column()
  age: number;

  @Column()
  document: string;

  @OneToMany(type => Movie, movie => movie.director)
  directedMovies: Movie[];

  @ManyToMany(type => Movie, movie => movie.actores)
  movies: Movie[];

  @CreateDateColumn({ name: 'created_at'})
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at'})
  updatedAt: Date;
}
