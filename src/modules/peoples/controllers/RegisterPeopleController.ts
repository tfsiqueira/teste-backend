import { Request, Response } from "express";
import RegisterPeopleService from "../services/RegisterPeopleService";

export default class RegisterPeopleController {
  async register(request: Request, response: Response): Promise<Response> {
    const { name, age, document } = request.body;

    const registerPeopleService = new RegisterPeopleService();

    const people = await registerPeopleService.execute({
      name,
      age,
      document
    });

    return response.status(201).json(people);
  }
}
