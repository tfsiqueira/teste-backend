import { Request, Response } from "express";
import ExceptionError from "../../../shared/errors/ExceptionError";
import SearchByNameService from "../services/SearchByNameService";

export default class SearchPeopleController {
  async searchByName(request: Request, response: Response): Promise<Response> {
    const name = request.query.name?.toString();

    if (!name) {
      throw new ExceptionError("Please enter a name");
    }

    const searchPeopleService = new SearchByNameService();

    const peoples = await searchPeopleService.execute(name);

    return response.status(201).json(peoples);
  }
}
