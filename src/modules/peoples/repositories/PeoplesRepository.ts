import { EntityRepository, getRepository, Repository } from "typeorm";
import ICreatePeopleDTO from "../dtos/IRegisterPeople.dto";
import People from "../models/People";
import IPeoplesRepository from "./IPeoplesRepository";

@EntityRepository(People)
export default class PeoplesRepository implements IPeoplesRepository {
  private ormRepository: Repository<People>;

  constructor() {
    this.ormRepository = getRepository(People);
  }

  async findById(id: string): Promise<People | undefined> {
    const people = await this.ormRepository.findOne(id);

    return people;
  }

  async findByDocument(document: string): Promise<People | undefined> {
    const people = await this.ormRepository.findOne({ where: {
      document
    }});

    return people;
  }

  async create(peopleData: ICreatePeopleDTO): Promise<People> {
    const people = this.ormRepository.create(peopleData);

    await this.ormRepository.save(people);

    return people;
  }

  async save(people: People): Promise<People> {
    return await this.ormRepository.save(people);
  }

  async findByName(name: string): Promise<People[]> {
    const peoples = await this.ormRepository.createQueryBuilder()
      .where("LOWER(name) LIKE :name", { name: `%${ name.toLowerCase() }%`})
      .getMany()

    return peoples;
  }

  async findManyPeoples(ids: string[]): Promise<People[]> {
    let peoples: People[] = [];

    for (const id of ids) {
      const people = await this.findById(id);
      if (people) {
        peoples.push(people);
      }
    }

    return peoples;

  }
}
