import ICreatePeopleDTO from "../dtos/IRegisterPeople.dto";
import People from "../models/People";

export default interface IPeoplesRepository {
  findById(id: string): Promise<People | undefined>;
  findByDocument(document: string): Promise<People | undefined>;
  create(data: ICreatePeopleDTO): Promise<People>;
  save(user: People): Promise<People>;
  findByName(name: string): Promise<People[]>;
  findManyPeoples(ids: string[]): Promise<People[]>;
}
