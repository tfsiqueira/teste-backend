import { NextFunction, Request, Response } from 'express';
import { getRepository } from 'typeorm';
import ExceptionError from '../../../shared/errors/ExceptionError';
import { UsersRole } from '../enums/usersRole';
import User from '../models/User';

export const userIsUserRole = async (
  request: Request,
  response: Response,
  next: NextFunction
): Promise<void> => {
  const id = request.user.id;

  const usersRepository = getRepository(User);

  const user = await usersRepository.findOneOrFail(id);

  if (user.role !== UsersRole.user) {
    throw new ExceptionError('This operation is only allowed by users Role', 403);
  }

  return next();
}
