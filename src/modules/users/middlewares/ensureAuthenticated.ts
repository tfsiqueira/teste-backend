import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import ExceptionError from '../../../shared/errors/ExceptionError';
import TokenPayload from '../dtos/sessions/ITokenSessions';

export default function ensureAuthenticated(
  request: Request,
  response: Response,
  next: NextFunction
): void {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    throw new ExceptionError('JWT token is missing');
  };

  const [, token] = authHeader.split(' ');

  try {
    const decoded = jwt.verify(token, `${process.env.JWT_PUBLIC_KEY}`, {
      algorithms: ['RS256']
    });

    const { sub } = decoded as TokenPayload;

    request.user = {
      id: sub,
    }

    return next();
  } catch (error) {
    throw new ExceptionError('Invalid JWT Token');
  }
}
