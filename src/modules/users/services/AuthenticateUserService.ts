import UsersRepository from '../repositories/UsersRepository';
import * as bcryptjs from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import ICreateSessionDTO from '../dtos/sessions/ICreateSessions.dto';
import IResponseSessionDTO from '../dtos/sessions/IResponseSession.dto';
import { UsersStatus } from '../enums/usersStatus';
import ExceptionError from '../../../shared/errors/ExceptionError';

export default class AuthenticateUserService {
  async execute({email, password}: ICreateSessionDTO): Promise<IResponseSessionDTO> {
    const usersRepository = new UsersRepository();

    const user = await usersRepository.findByEmail(email);

    if (user && user.status !== UsersStatus.active) {
      throw new ExceptionError('Incorrect email/password combination.');
    }

    if (!user) {
      throw new ExceptionError('Incorrect email/password combination.');
    }

    const passwordMatched = await bcryptjs.compare(
      password,
      user.password
    );

    if (!passwordMatched) {
      throw new ExceptionError('Incorrect email/password combination.');
    };

    const token = jwt.sign({}, `${process.env.JWT_PRIVATE_KEY}` , {
      subject: user.id,
      expiresIn: 86400,
      algorithm: 'RS256'
    });

    const payload = {
      tokenType: "Bearer",
      token,
      expiresIn: '86400'
    }

    return payload;
  }
}
