import ExceptionError from "../../../shared/errors/ExceptionError";
import { UsersStatus } from "../enums/usersStatus";
import UsersRepository from "../repositories/UsersRepository";

export default class DisableUserService {
  async execute(userId: string): Promise<void> {
    const usersRepository = new UsersRepository();

    const user = await usersRepository.findById(userId);

    if (!user) {
      throw new ExceptionError('User not Found', 404);
    }

    user.status = UsersStatus.inactive;

    try {
      await usersRepository.save(user);
    } catch (error) {
      throw new ExceptionError('There was an error deleting the user.');
    }
  }
}
