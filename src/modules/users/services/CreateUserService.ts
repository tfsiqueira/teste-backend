import ICreateUserDTO from "../dtos/users/ICreateUser.dto";
import UsersRepository from "../repositories/UsersRepository";

import * as bcryptjs from 'bcryptjs';
import { UsersStatus } from "../enums/usersStatus";
import IReturnUserDTO from "../dtos/users/IReturnUser.dto";
import ExceptionError from "../../../shared/errors/ExceptionError";

export default class CreateUserService {
  async execute({name, email, password, role}: ICreateUserDTO): Promise<IReturnUserDTO> {
    const usersRepository = new UsersRepository();

    const userAlreadyExist = await usersRepository.findByEmail(email);

    if (userAlreadyExist) {
      throw new ExceptionError('Email already registered!');
    }

    const passwordHashed = await bcryptjs.hash(password, 8);

    const user = await usersRepository.create({
      name,
      email,
      password: passwordHashed,
      role,
      status: UsersStatus.active
    });

    const payload = {
      id: user.id,
      name: user.name,
      email: user.email,
      role: user.role,
      status: user.status
    } as IReturnUserDTO;

    return payload;
  }
}
