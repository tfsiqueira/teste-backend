import ExceptionError from "../../../shared/errors/ExceptionError";
import IReturnUserDTO from "../dtos/users/IReturnUser.dto";
import IUpdateUserDTO from "../dtos/users/IUpdateUser.dto";
import UsersRepository from "../repositories/UsersRepository";

export default class UpdateUserService {
  async execute({userId, name, email}: IUpdateUserDTO): Promise<IReturnUserDTO> {
    const usersRepository = new UsersRepository();

    const user = await usersRepository.findById(userId);

    if (!user) {
      throw new ExceptionError('User not Found', 404);
    }

    user.name = name;
    user.email = email;

    try {
      await usersRepository.save(user);
    } catch (error) {
      throw new ExceptionError('An error occurred while updating the user.');
    }

    const payload = {
      id: user.id,
      name: user.name,
      email: user.email,
      role: user.role,
      status: user.status
    } as IReturnUserDTO;

    return payload;
  }
}
