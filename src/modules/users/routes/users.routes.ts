import { Router } from 'express';
import DisableUsersController from '../controllers/DisableUsersController';
import UpdateUsersController from '../controllers/UpdateUsersController';
import UsersController from '../controllers/UsersController';
import ensureAuthenticated from '../middlewares/ensureAuthenticated';

const usersRouter = Router();
const userController = new UsersController();
const disableUserController = new DisableUsersController();
const updateUserController = new UpdateUsersController();

usersRouter.post('/users/register', userController.create);
usersRouter.patch('/users/delete', ensureAuthenticated, disableUserController.disable);
usersRouter.put('/users/update', ensureAuthenticated, updateUserController.update);

export default usersRouter;
