import { EntityRepository, getRepository, Repository } from "typeorm";
import ISaveUserDTO from "../dtos/users/ISaveUser.dto";
import User from "../models/User";
import IUsersRepository from "./IUsersRepository";

@EntityRepository(User)
export default class UsersRepository implements IUsersRepository {
  private ormRepository: Repository<User>;

  constructor() {
    this.ormRepository = getRepository(User);
  }

  async findById(id: string): Promise<User | undefined> {
    const user = await this.ormRepository.findOne(id);

    return user;
  }

  async findByEmail(email: string): Promise<User | undefined> {
    const user = await this.ormRepository.findOne({
      where: { email },
    });

    return user;
  }

  async create(userData: ISaveUserDTO): Promise<User> {
    const user = this.ormRepository.create(userData);

    await this.ormRepository.save(user);

    return user;
  }

  async save(user: User): Promise<User> {
    return await this.ormRepository.save(user);
  }
}
