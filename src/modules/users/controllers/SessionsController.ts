import { Request, Response } from 'express';
import AuthenticateUserService from '../services/AuthenticateUserService';

export default class SessionsController {
  async create(request: Request, response: Response): Promise<Response> {
    const { email, password } = request.body;

    const sessionsService = new AuthenticateUserService();

    const payload = await sessionsService.execute({
      email,
      password
    });

    return response.json(payload);
  }
}
