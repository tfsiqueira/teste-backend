import { Request, Response } from 'express';
import DisableUserService from '../services/DisableUserService';

export default class DisableUsersController {
  async disable(request: Request, response: Response): Promise<Response> {
    const userId = request.user.id;

    const disableUserService = new DisableUserService();

    await disableUserService.execute(userId);

    return response.status(200).json();
  }
}
