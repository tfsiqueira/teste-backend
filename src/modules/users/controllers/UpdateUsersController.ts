import { Request, Response } from 'express';
import UpdateUserService from '../services/UpdateUserService';

export default class UpdateUsersController {
  async update(request: Request, response: Response): Promise<Response> {
    const { name, email } = request.body;
    const userId = request.user.id;

    const updateUserService = new UpdateUserService();

    const updatedUser = await updateUserService.execute({
      userId,
      name,
      email
    });

    return response.status(200).json(updatedUser);
  }
}
