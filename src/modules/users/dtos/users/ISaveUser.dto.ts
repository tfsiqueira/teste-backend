export default interface ISaveUserDTO {
  name: string;
  email: string;
  password: string;
  role: string;
  status: string;
}
