export default interface IReturnUserDTO {
  id: string,
  name: string,
  email: string,
  role: string,
  status: string
}
