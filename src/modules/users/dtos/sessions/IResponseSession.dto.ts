export default interface IResponseSessionDTO {
  tokenType: string;
  token: string;
  expiresIn: string;
};
